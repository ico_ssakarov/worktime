
<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
require 'connect.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
session_cache_limiter(false);
session_start();
$app = new \Slim\Slim();

// $app->add(new Slim\Middleware\SessionCookie());
/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function.
 */

$app->get('/', function(){
    header('Location: /app/index.html');
});
$app->post('/user/login', 'logIn');
$app->post('/user/singup', 'singUp');
$app->get('/user/logout', 'logOut');
$app->get('/user/isauth', 'isAuth');
$app->get('/user/startday', 'startDay');
$app->get('/user/stopday', 'stopDay');
$app->get('/user/getjsonforchart', 'getJsonforChart');
$app->get('/user/generate', 'generate');

function isAuth(){
    if(!empty($_SESSION['is_logged_in'])){
        echo json_encode(array('auth'=>1)); 
    }else{
        echo json_encode(array('auth'=>0)); 
    }
}

function logIn(){
    $post = json_decode(file_get_contents('php://input'),1);
    $db = new dbConnect();
    $db = $db->connect();
    $sql = 'SELECT * FROM users WHERE login=:login AND password=:password';
    $stmt = $db->prepare($sql);
    $stmt->bindParam("login", $post['user']['login']);
    $stmt->bindParam("password", md5($post['user']['pass']));
    $stmt->execute();
    $user = $stmt->fetchObject();
    if($user){
        $_SESSION['is_logged_in'] = true;
        $_SESSION['user_id'] = $user->id;
        echo json_encode(array('auth'=>1)); 
    }
    // // $res = $db->query($sql);
    // $user = $res->fetchAll(PDO::FETCH_OBJ);
}

function logOut(){
    session_unset();
}

function singUp(){
    $post = json_decode(file_get_contents('php://input'),1);
    $db = new dbConnect();
    $db = $db->connect();
    $sql = 'INSERT INTO users (full_name, login, password, position, age) VALUES(:full_name, :login, :password, :position, :age)';
    $stmt = $db->prepare($sql);
    $stmt->bindParam("full_name", $post['user']['full_name']);
    $stmt->bindParam("login", $post['user']['login']);
    $stmt->bindParam("password", md5($post['user']['pass']));
    $stmt->bindParam("position", $post['user']['position']);
    $stmt->bindParam("age", $post['user']['age']);
    if($stmt->execute()){
        $_SESSION['is_logged_in'] = true;
        $_SESSION['user_id'] = $db->lastInsertId();
        echo json_encode(array('auth'=>1)); 
    }
}   

function startDay(){
    $user_id = (int)$_SESSION['user_id'];
    $db = new dbConnect();
    $db = $db->connect();
    $sql = 'INSERT INTO working_time (user_id, start, end) VALUES(:user_id, :start, :end)';
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $user_id);
    $stmt->bindParam("start", time());
    $stmt->bindParam("end", time());
    if($stmt->execute()){
        $_SESSION['work_id'] = $db->lastInsertId();
    }
}

function stopDay(){
    if(isset($_SESSION['work_id'])){
        $id = $_SESSION['work_id'];
        $db = new dbConnect();
        $db = $db->connect();
        $sql = "UPDATE working_time SET end=:end WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->bindParam("end", time());
        if($stmt->execute()){
            unset($_SESSION['work_id']);
        }
    }
}

function getJsonforChart(){
    $db = new dbConnect();
    $db = $db->connect();
    $id = (int)$_SESSION['user_id'];
    $sql = 'SELECT * FROM working_time WHERE user_id=:user_id ORDER BY start';
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $id);
    $stmt->execute();
    $workTime = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach ($workTime as $value) {
        $hour = ($value->end-$value->start)/60/60;
        $arr[] = array($value->start*1000, $hour);
    }
    echo json_encode($arr);
}

function generate(){
    $db = new dbConnect();
    $db = $db->connect();
    $id = (int)$_SESSION['user_id'];
    $arr = array(6,4,8,10,3,5,9);
    for($i = 1; $i<=100; $i++){
        $timeEnd = time()+86400*$i;
        $hours = $arr[array_rand($arr)];
        $timeStart = $timeEnd-$hours*60*60;
        $sql = 'INSERT INTO working_time (user_id, start, end) VALUES(:user_id, :start, :end)';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $id);
        $stmt->bindParam("start", $timeStart);
        $stmt->bindParam("end", $timeEnd);
        $stmt->execute();
        echo $i.'<br>';
    }
}



/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
