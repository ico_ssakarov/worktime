'use strict';

angular.module('angularApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.init = function(){
    	$scope.chart.init();
      $scope.user.isAuth();
    }

    $scope.user = {
    	auth: 0,

      isAuth: function(){
        var self = this;
        $http.get('/user/isauth').success(function(res){
          if(res.auth === 1)
            self.auth = 1;
          else
            self.auth = 0;
        })
      },

  		singIn: function(){
        var self = this;
  			$http.post('/user/login', {
  				user:{login:this.login, pass:this.password}
  			}).success(function(res){
          if(res.auth === 1){
            self.auth = 1;
            $scope.chart.init();
          }
        })
  		},

  		logout: function(){
  			$http.get('/user/logout').success(function(res){
          if(res)
            this.auth = 0;
        })
  		},

  		singUp: function(){
        var self = this; 
  			$http.post('/user/singup', {
  				user:{
  					login 	  : this.newLogin, 
  					pass  	  : this.newPassword,
  					full_name : this.name+' '+this.lastname,
  					position  : this.position
  				}
  			}).success(function(res){
          self.auth = res.auth;
        })
  			// this.auth = 1;
  		},
    }

    $scope.day = {
    	on:0,

    	start: function(){
        var self = this;
    		$http.get('/user/startday').success(function(res){
          $scope.timer.start();
          self.on = 1;
        })
    	},

    	end: function(){
        var self = this;
    		$http.get('/user/stopday').success(function(res){
          $scope.timer.stop();
          self.on = 0;
        })
    	}
    }

   	$scope.timer = {
   		s:0,
   		m:0,
   		h:0,
   		timer:{},

   		start: function(){
   			var self = this;
   			this.timer = setInterval(function(){
   				$scope.$apply(function(){
	   				self.s++;
	   				if(self.s === 59){
	   					self.m++;
	   					self.s = 0;
	   				}
	   				if(self.m === 59){
	   					self.h++;
	   					self.m = 0;
	   				}
   				})
   			},1000)
   		},

   		stop: function(){
   			clearInterval(this.timer);
   			this.s = 0;
   			this.m = 0;
   			this.h = 0;
   		}
   	}

   	$scope.chart = {
        data: [],

        init: function(){
        	var self = this;
        	$http.get('/user/getjsonforchart').success(function(res){
          if(res){
            self.data = res;
            self.paint();
          }
        })
        },

        paint: function(){
        	$('#container').highcharts({
        	    chart: {
        	        type: 'spline'
        	    },
        	    title: {
        	        text: 'График работы'
        	    },
        	    xAxis: {
        	        type: 'datetime',
        	        dateTimeLabelFormats: { 
        	            day: '%e. %b',
        	            month: '%e. %b'
        	        }
        	    },
        	    yAxis: {
        	        title: {
        	            text: 'Часы работы'
        	        },
        	        min: 0
        	    },
        	    tooltip: {
        	        formatter: function() {
        	            return Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y +' часов';
        	        }
        	    },
        	    
        	    series: [{
        	        name: new Date().getFullYear(),
        	        data: this.data
        	    }]
        	});
        }
	}
  //Инициализируем приложение
  $scope.init();
});
